<?php
ini_set('display_errors', "On");

require_once('http.php');


$api_key = '8c2f21fb825dccf92544e7f8d7005cf26c91b1af';
$host = '172.16.223.10:8443';
$sensor_id = 33;

$reg = 'HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Services\SharedAccess\Parameters\FirewallPolicy\DomainProfile';

$lr = new CBRLR($api_key, $host);
$lr->newSession($sensor_id);
$lr->waitTillActive();

$result = $lr->cmdRegEnumKey($reg);
print_r(json_decode($result, TRUE));




class CBRLR
{
	private $api_key;
	private $host;
	private $header_ar;
	private $session_id;
	private $sensor_id;

	private $sensor_session_ar;

	private $path_session = '/api/v1/cblr/session'; 

	function __construct($api_key, $host)
	{
		$this->api_key = $api_key;
		$this->host = $host;
		$this->mkAuthHeader();
	}


	function newSession($sensor_id)
	{
		$this->sensor_id = $sensor_id;
		$body = '{"sensor_id":' . $sensor_id . '}';
		$url = $this->mkURL($this->path_session);
		$result = HTTPreq('POST', $url, $body, $this->header_ar);
		$this->new_session_json = $result;
//		return $result;

	}

	function waitTillActive($wait_sec = 2)
	{
		while(TRUE)
		{
			$result = $this->getSessions();
			$sensor_status = $this->checkActive($result);
			if($sensor_status === TRUE)
				break;

			echo 'status is ' . $sensor_status . "\n";

			sleep($wait_sec);
		}
		$this->getSessionID($result);
	}
        function checkActive($session_ar)
        {
                $session_ar = json_decode($session_ar, TRUE);
                foreach($session_ar as $sensor_ar)
                {
                        if($sensor_ar['sensor_id'] == $this->sensor_id)
                        {
				$this->sensor_session_ar = $sensor_ar; //override many time. here is bad place to set val....
                                if($sensor_ar['status'] === 'active')
 	                        	return TRUE;
				else
					return $sensor_ar['status'];
                        }
                }
		return FALSE;

        }


	function getSessionID($session_ar)
	{
		$session_ar = json_decode($session_ar, TRUE);
		foreach($session_ar as $sensor_ar)
		{
			if($sensor_ar['sensor_id'] == $this->sensor_id)
			{
				$this->setSessionID($sensor_ar['id']);
				return;
			}
		}

	}


	function cmdRegEnumKey($reg)
	{
		$reg = str_replace('\\', '\\\\', $reg);
		$body = '{"name":"reg enum key","object":"' . $reg . '"}';
//var_dump($body);
		$url = $this->mkURL($this->path_session . '/' . $this->session_id . '/command?wait=true');
			
		$result = HTTPreq('POST', $url, $body, $this->header_ar);

		$res_ar = json_decode($result, TRUE);
//print_r($res_ar);		
		$cmd_id = $res_ar['id'];

		return $this->getResult($cmd_id);

	}

	function getResult($cmd_id)
	{
		//https://172.16.223.10:8443/api/v1/cblr/session/29/command/6?wait=true

		
		$url = $this->mkURL($this->path_session . '/' . $this->session_id . '/command/' . $cmd_id . '?wait=true');

		return  HTTPreq('GET', $url, FALSE, $this->header_ar);
		
	}

	function __destruct()
	{
		$this->closeSession();
	}


	function closeSession()
	{
		$this->sensor_session_ar['status'] = 'close';
		//$this->sensor_session_ar['create_time'] = time();
		$body = json_encode($this->sensor_session_ar);
		$url = $this->mkURL($this->path_session . '/' . $this->session_id);
		HTTPreq('PUT', $url, $body, $this->header_ar);

	}


	function mkAuthHeader()
	{
		$this->header_ar = array('Content-Type: application/json', 'X-Auth-Token:' . $this->api_key);
	}

	function addHeader($str)
	{
		$this->header_ar[] = $str;
	}

	function getSessions()
	{
		$url = $this->mkURL($this->path_session . '?active_only=true');
		$result = HTTPreq('GET', $url, FALSE, $this->header_ar);

		return $result;
	}

	function setSessionID($session_id)
	{
//	var_dump($session_id);
		$this->session_id = $session_id;
	}

	function mkCommandURL()
	{
		$session_path = $this->path_session . '/' . $this->session_id . '/command?wait=true';
		return $this->mkURL($session_path);
	}

	function mkURL($path)
	{
		return $url = 'https://' . $this->host . $path;
	}

}






