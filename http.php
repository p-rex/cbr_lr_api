<?php

define('SHOW_HTTP_HEADER', FALSE);
//define('SHOW_HTTP_HEADER', TRUE);

function HTTPreq($method, $url, $post_data = FALSE, $header_ar = FALSE)
{
	$curl_id = curl_init();
	curl_setopt($curl_id, CURLOPT_URL, $url);//URLをセット
	curl_setopt($curl_id, CURLOPT_RETURNTRANSFER, TRUE); //これが無いと標準出力に出力する。PHPマニュアルはウソが書いてある。

	if(SHOW_HTTP_HEADER)
	{
		curl_setopt($curl_id, CURLINFO_HEADER_OUT,TRUE); //Request Header
		curl_setopt($curl_id, CURLOPT_HEADER, TRUE); //Response Header
	}
	if($method === 'POST')
	{
		curl_setopt($curl_id, CURLOPT_POST, 1);
		curl_setopt($curl_id, CURLOPT_POSTFIELDS, $post_data);
	}

	if($method === 'PUT')
	{
		curl_setopt($curl_id, CURLOPT_CUSTOMREQUEST, 'PUT'); // ※
		curl_setopt($curl_id, CURLOPT_POSTFIELDS, $post_data); // ※

	}


	if($header_ar)
		curl_setopt($curl_id, CURLOPT_HTTPHEADER, $header_ar);

	//SSL
	list($scheme, $path) = explode(':', $url, 2);
	if($scheme === 'https')
	{
//		curl_setopt($curl_id, CURLOPT_SSLVERSION, 3);
		curl_setopt($curl_id, CURLOPT_SSL_VERIFYPEER, FALSE); //サーバ証明書検証を無視
		curl_setopt($curl_id, CURLOPT_SSL_VERIFYHOST, FALSE); //サーバ証明書検証を無視
	}
	$resp = curl_exec($curl_id);
	if(curl_errno($curl_id) != 0)
	{
		$error_msg = curl_error($curl_id);
		curl_close($curl_id);
		// 接続ができない場合の処理（エラー処理)
		die($error_msg);
	}
	else
	{
		echo curl_getinfo($curl_id, CURLINFO_HEADER_OUT);
		//$statusCode = curl_getinfo($curl_id, CURLINFO_HTTP_CODE);
		//var_dump($statusCode);
		curl_close($curl_id);
		return $resp;
	}
}
